# README #

### Thesis demo application ###

This repository contains demo application project, which is a practical part of my thesis("Facial animation tool").

### Structure ###

This project uses 3d model from [series of tutorial](https://cgi.tutsplus.com/tutorials/create-a-facial-animation-setup-in-blender-part-1--cg-32251).
UI of this project was build using [BGUI](http://bgui.readthedocs.io/en/latest/)

### How do I get set up? ###

In order to use this project you need to have installed Blender.
This project was done in Blender 2.78. It was tested under MacOS and Windows OS.
import sys
import bgui
import bgui.bge_utils
import bge
import aud
import os.path

class SimpleLayout(bgui.bge_utils.Layout):

	def __init__(self, sys, data):
		super().__init__(sys, data)
		
		self.frame = bgui.Frame(self, border=0)
		self.frame.colors = [(0, 0, 0, 0) for i in range(4)]

		self.win = bgui.Frame(self, size=[1, 1],
			options=bgui.BGUI_DEFAULT|bgui.BGUI_CENTERED)

		self.win.img1 = bgui.Image(self.win, "images/vowel.jpg", size=[.1237, .5585], pos=[0, 0.442],
			options = bgui.BGUI_DEFAULT|bgui.BGUI_CACHE)
		self.win.img2 = bgui.Image(self.win, "images/consonant.jpg", size=[.155, 1], pos=[.845, 0],
			options = bgui.BGUI_DEFAULT|bgui.BGUI_CACHE)
		self.win.img3 = bgui.Image(self.win, "images/input.jpg", size=[0.279, .064], pos=[0, 0],
			options = bgui.BGUI_DEFAULT|bgui.BGUI_CACHE)
		self.win.img4 = bgui.Image(self.win, "images/emotion.jpg", size=[0.1512, .4485], pos=[0.122, 0.552],
			options = bgui.BGUI_DEFAULT|bgui.BGUI_CACHE)

		self.input = bgui.TextInput(self.win, text="1", color=(0,0,0,0), pt_size = 40, size=[.1, .04], pos=[.17, 0.016],
			input_options = bgui.BGUI_INPUT_NONE, options = bgui.BGUI_DEFAULT)
		self.button = bgui.FrameButton(self.win, text='Play', pt_size = 35, size=[.061, .054], pos=[.2154, .0045],
			options = bgui.BGUI_DEFAULT)
		self.button.on_click = self.on_play_click

	def on_play_click(self, widget):
		soundFileName = "sounds/" + self.input.text + ".wav"
		if os.path.isfile(soundFileName):
			device = aud.device()
			sound = aud.Factory.file(soundFileName)
			sound = sound.pitch(2)
			device.play(sound)
		controller = bge.logic.getCurrentController()
		actPhoneme = controller.actuators["Actuator"]
		actPhoneme.action = "Action" + self.input.text
		actPhoneme.frameStart = 1
		actPhoneme.frameEnd = 60
		actPhoneme.blendIn = 0
		actPhoneme.mode = bge.logic.KX_ACTIONACT_PLAY
		controller.activate(actPhoneme)

def main(cont):
	own = cont.owner
	mouse = bge.logic.mouse

	if 'sys' not in own:
		own['sys'] = bgui.bge_utils.System('../../themes/default')
		own['sys'].load_layout(SimpleLayout, None)
		mouse.visible = True

	else:
		own['sys'].run()
